package com.mailcheck.client

import org.apache.log4j.Logger

import javax.mail.*

/**
 * timo@klarshift.de
 * mail checker client
 */
class MailCheckerClient {
    private final Logger log = Logger.getLogger(this.class)
    private final String host
    private int port = 993
    private String password, username
    private Properties mailProperties

    private Session session
    private Store store

    /**
     * create a new mail checker client
     * @param host
     * @param port
     * @param username
     * @param password
     */
    public MailCheckerClient(String host, int port, String username, String password){
        this.host = host
        this.port = port
        this.username = username
        this.password = password

        init();
    }

    /**
     * init the client
     */
    private void init(){
        // create properties
        mailProperties = new Properties()
        mailProperties.setProperty("mail.store.protocol", "imaps")
        mailProperties.setProperty("mail.imaps.host", host)
        mailProperties.setProperty("mail.imaps.port", port.toString())

        log.info "created mail checker client"
    }

    /**
     * connect to server
     */
    public void connect(){
        log.info "connecting to $username@$host"

        session = Session.getDefaultInstance(mailProperties,null)
        store = session.getStore("imaps")
        store.connect(host, username, password)

        log.info "connected"
    }

    /**
     * disconnect from server
     */
    public void disconnect(){
        store.close()
        log.info "connection closed."
    }

    /**
     * check an account
     *
     * @param inboxFolder
     * @param appId
     * @param maxAgeInSeconds
     * @return
     */
    public boolean checkAccount(String inboxFolder, String appId, long maxAgeInSeconds = 300 ){
        log.info "checking account $appId ($inboxFolder)"

        // get inFolder and open it
        Folder inFolder = store.getFolder(inboxFolder)
        inFolder.open(Folder.READ_ONLY)

        // check inbox message count
        int messageCount = inFolder.messageCount
        if(messageCount == 0){
            log.info "no messages in $inboxFolder"
            return false
        }

        log.info "having $messageCount mails in $inboxFolder"

        long minAge = Long.MAX_VALUE

        for(int m=messageCount; m>0; m--){
            Message msg = inFolder.getMessage(m)
            long age = (System.currentTimeMillis() - msg.receivedDate.time) / 1000
            if(msg.subject.contains("Mail Check [$appId]") ){
                if(age < maxAgeInSeconds){
                    log.info "checked mail successfully: `${msg.subject}` received $age seconds ago "
                    return true
                }

                if(age < minAge)
                    minAge = age

            }
        }

        log.info "test failed: last email for $appId received ${minAge} seconds ago"

        inFolder.close(true)

        return false
    }

    public void cleanFolder(String inboxFolder){

        // get inFolder and open it
        Folder inFolder = store.getFolder(inboxFolder)
        inFolder.open(Folder.READ_WRITE)

        // check inbox message count
        int messageCount = inFolder.messageCount
        if(messageCount == 0){
            return
        }

        log.info "cleaning folder $inboxFolder ..."


        long maxAge = 60*60

        for(int m=messageCount; m>0; m--){
            Message msg = inFolder.getMessage(m)
            long age = (System.currentTimeMillis() - msg.receivedDate.time) / 1000

            if(age > maxAge){
                msg.setFlag(Flags.Flag.DELETED, true);
                log.info "deleted #$m - ${msg.subject}"
            }
        }

        inFolder.close(true)
    }
}
