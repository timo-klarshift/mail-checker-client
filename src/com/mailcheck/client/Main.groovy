package com.mailcheck.client

import org.apache.log4j.ConsoleAppender
import org.apache.log4j.Level
import org.apache.log4j.Logger
import org.apache.log4j.SimpleLayout

/**
 *
 * User: timo (timo@klarshift.de)
 * Date: 30.10.13
 * Time: 15:03
 */
class Main {
    static final Logger log = Logger.getLogger(Main.class)

    public static void main(String[] args){
        // set logging
        Logger.getRootLogger().addAppender(new ConsoleAppender(new SimpleLayout()))
        Logger.getRootLogger().setLevel(Level.DEBUG)

        // parse arguments
        Map options = [
                defaults: [
                        port: 993,
                        folder: "INBOX"
                ]
        ] + parseOptions(args)

        Set<String> cleanFolders = new HashSet<String>()

        if(options.config){
            File configFile = new File(options.config)
            if(configFile.exists() == false){
                log.error "file does not exist: $configFile"
                return
            }

            def xml = new XmlSlurper().parse(configFile)

            // defaults
            options.defaults.timeout = Integer.parseInt(xml.defaults.timeout.text()) ?: options.defaults.timeout
            options.defaults.folder = xml.defaults.folder.text() ?: options.defaults.folder
            options.defaults.fail = xml.defaults.fail.text()

            // imap
            options.host = xml.imap.host.text()
            options.username = xml.imap.username.text()
            options.password = xml.imap.password.text()
            options.port = Integer.parseInt(xml.imap.port.text())

            // projects
            options.apps = [:]
            xml.projects.project.each {
                options.apps[it.@id.text()] = [:]

                if(it.@folder.text() != ""){
                    options.apps[it.@id.text()].folder = it.@folder
                }

                if(it.@fail.text() != ""){

                    options.apps[it.@id.text()]['fail'] = it.@fail
                }





            }
        }

        // TODO validate options

        // create the client
        def client = new MailCheckerClient(options.host, options.port, options.username, options.password)
        client.connect()

        // check all app ids
        options.apps.each { app ->
            // get options for this project
            String appKey = app.key
            String folder = app.value.folder ?: options.defaults.folder
            String failHandler = app.value.fail ?: options.defaults.fail

            cleanFolders << folder

            // check imap account
            if( ! client.checkAccount(folder, appKey)){

                // callback failure handler
                if(failHandler){
                    def run = failHandler.split(' ') as List
                    run << "$appKey"
                    run.execute().waitForOrKill(10000L)
                }
            }
        }

        // clean all folders
        cleanFolders.each {
            client.cleanFolder(it)
        }

        // cleanup
        client.disconnect()
    }

    /**
     * parse options
     * @param args
     * @return
     */
    private static Map parseOptions(String[] args){
        Map arguments = [:]

        String lastAppId = null

        for(int i=0; i<args.length; i++){
            if(args[i] == '--config'){
                arguments.config = args[i+1]
                i++
            }else if(args[i] == '--host'){
                arguments.host = args[i+1]
                i++
            }else if(args[i] == '--port'){
                arguments.port = Integer.parseInt(args[i+1])
                i++
            }else if(args[i] == '--username'){
                arguments.username = args[i+1]
                i++
            }else if(args[i] == '--password'){
                arguments.password = args[i+1]
                i++
            }else if(args[i] == '--app'){
                arguments.apps = arguments.apps ?: [:]
                arguments.apps[args[i+1]] = [:]
                lastAppId = args[i+1]
                i++
            }else if(args[i] == '--folder'){
                if(lastAppId != null){
                    arguments.apps[lastAppId].folder = args[i+1]
                    i++
                }
            }else if(args[i] == '--on-fail'){
                arguments.failHandler = args[i+1]
                i++
            }
        }

        return arguments
    }
}
